import React from 'react';
// import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom';
import Login from './components/Login';
import Wedding from './components/Wedding';
import CreateWedding from './components/CreateWedding';
import CreateExpense from './components/CreateExpense';
import EditExpense from './components/EditExpense';
import EditWedding from './components/EditWedding';
import GetWedding from './components/GetWedding';
import GetExpense from './components/GetExpense';
import Chat from './components/Chat';
import NotFound from './components/NotFound';

export default function AppRouter(){
    <div>
        if (!localStorage.getItem("token") === ""){
            <Router>
                <Switch>
                    <Route path="/wedding" component={Wedding} />
                    <Route path="/addwedding" component={CreateWedding} />
                    <Route path="/addexpense/:id" component={CreateExpense} />
                    <Route path="/editexpense/:id" component={EditExpense} />
                    <Route path="/editwedding/:id" component={EditWedding} />
                    <Route path="/getwedding/:id" component={GetWedding} />
                    <Route path="/getexpense/:id" component={GetExpense} />
                    <Route path="/chat" component={Chat} />
                    <Route component={NotFound} /> 
                </Switch>
            </Router>
        } else {
            <Redirect to="/" component={Login} />
        }
        
    </div>
}