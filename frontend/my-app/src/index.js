import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Login from './components/Login';
import Wedding from './components/Wedding';
import CreateWedding from './components/CreateWedding';
import CreateExpense from './components/CreateExpense';
import EditExpense from './components/EditExpense';
import EditWedding from './components/EditWedding';
import GetWedding from './components/GetWedding';
import GetExpense from './components/GetExpense';
import Chat from './components/Chat';
import 'bootstrap/dist/css/bootstrap.css';
import Header from './components/Header';
import Footer from './components/Footer';

ReactDOM.render(
  <React.StrictMode>
    <Router>
      <Header/>
        <Route path="/" component={Login} exact={true} />
        
      
        <Route path="/wedding" component={Wedding} />
        

        <Route path="/addwedding" component={CreateWedding} />


        <Route path="/addexpense/:id" component={CreateExpense} />


        <Route path="/editexpense/:id" component={EditExpense} />
      

        <Route path="/editwedding/:id" component={EditWedding} />


        <Route path="/getwedding/:id" component={GetWedding} />


        <Route path="/getexpense/:id" component={GetExpense} />


        <Route path="/chat" component={Chat} />

        {/* <Footer /> */}
    </Router>
  </React.StrictMode>,
  document.getElementById('root')
);