import axios from "axios";
import { useRef, useState, useEffect } from "react";
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import MessageTable from "./MessageTable";


export default function Chat(){

    const useStyles = makeStyles({
        create: {
          background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
          border: 0,
          borderRadius: 3,
          boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
          color: 'white',
          height: 24,
          padding: '0 30px',
        },
      });
    
    const classes = useStyles();

    
    const [messages, setMessages] = useState([]);
    const [others, setOthers] = useState([]);
    
    const messageInput = useRef(null);
    const recipientInput = useRef(null);

    const testEmail = JSON.parse(localStorage.getItem('user info'));
    const loggedInEmail = testEmail.email;
    // console.log(loggedInEmail);


    async function getOthers(event){
        
        const response = await axios.get('https://authorization-service-dot-renwick-wedding-app.ue.r.appspot.com/employees');
        let theOthers = response.data;
        theOthers.filter(a => a.email !== loggedInEmail);
        setOthers(theOthers);
    }

    // useEffect(() =>{
    //     getOthers();
    // });
    
    async function createMessage(event){
        const message = {
            note: messageInput.current.value,
            sender: loggedInEmail,
            recipient: recipientInput.current.value
        }
        const response = await axios.post(`https://messaging-service-dot-renwick-wedding-app.ue.r.appspot.com/messages`, message);
        alert("New Message Added!");
    }

    async function getAllMessages(event){
        const response = await axios.get(`https://messaging-service-dot-renwick-wedding-app.ue.r.appspot.com/messages`);
        setMessages(response.data);
    }
    

    return(<div>
        <h1 style={{ "font-family": 'Glory' }}>Chat Room</h1>
        <br />
        <button type="button" class="btn btn-outline-warning" onClick={getOthers}>Get Others</button>
        <br/>
        <br/>
        <hr/>
        

        <textarea name="message" cols="50" rows="10" ref={messageInput} minLength="5" required></textarea>
        <br/>
        <select placeholder="Recipient" required>

        {others.map(o => <option value={o.email} ref={recipientInput}>{o.email}</option>)}

        </select>
        <br />
        <br />
        <br />
        <button type="button" class="btn btn-outline-primary" onClick={createMessage}>Send Message</button>
        <button type="button" class="btn btn-outline-success" onClick={getAllMessages}>Get All Messages</button>
        <br />
        <br />
        <hr />
        <br />
        <br />
        <h2>All Messages</h2>
        <hr />
        <MessageTable messages={messages} getAllmessages={getAllMessages}>
        </MessageTable>

    </div>)
}