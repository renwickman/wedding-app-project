import axios from "axios";
import { useState, useRef } from "react";
import { useParams } from "react-router";


export default function EditWedding(props){
    const [wedding, setWedding] = useState([]);
    const groomInput = useRef(null);
    const brideInput = useRef(null);
    const locationInput = useRef(null);
    const budgetInput = useRef(null);
    const dateInput = useRef(null);


    const { id } = useParams();

    async function getWedding(id){
        const response = await axios.get(`http://34.66.174.184:3004/weddings/${id}`);
        console.log(response);
        setWedding(response.data);
    }

    getWedding(id);

    async function editWedding(event){
        const newWedding = {
            groomName: groomInput.current.value,
            brideName: brideInput.current.value,
            weddingLocation: locationInput.current.value,
            weddingBudget: budgetInput.current.value,
            weddingDate: dateInput.current.value
        }
        const response = await axios.put(`http://34.66.174.184:3004/weddings/${id}`, newWedding);
        alert("Wedding Edited!");
        setWedding(response.data);
    }

    return(
    
    <div style={{ "font-family": 'Glory' }}>
        <input placeholder={wedding.groomName} ref={groomInput}></input>
        <br/>
        <input placeholder={wedding.brideName} ref={brideInput}></input>
        <br/>
            <select placeholder={wedding.weddingLocation} ref={locationInput}>
                <option value="Dallas, TX">Dallas, TX</option>
                <option value="Los Angeles, CA">Los Angeles, CA</option>
                <option value="New York City, NY">New York City, NY</option>
                <option value="Detroit, MI">Detroit, MI</option>
                <option value="Miami, FL">Miami, FL</option>
                <option value="Austin, TX">Austin, TX</option>
                <option value="St. Louis, MO">St. Louis, MO</option>
                <option value="Chicago, IL">Chicago, IL</option>
                <option value="Houston, TX">Houston, TX</option>
                <option value="Atlanta, GA">Atlanta, GA</option>
                <option value="Las Vegas, NV">Las Vegas, NV</option>
                <option value="Seattle, WA">Seattle, WA</option>
            </select>
        <input placeholder={wedding.weddingBudget} ref={budgetInput} type="number"></input>
        <input placeholder={wedding.weddingDate} ref={dateInput} type="date"></input>
        <button onClick={editWedding}>Edit Wedding 💒</button>
        <br/>
        <h3>Current Date: {new Date(wedding.weddingDate).toDateString()}</h3>
        <h3>Current Location: {wedding.weddingLocation}</h3>
    </div>
    )
}
