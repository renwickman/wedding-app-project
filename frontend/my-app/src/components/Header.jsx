// import { makeStyles } from '@material-ui/core/styles';
import React from 'react';
import { LinkContainer } from 'react-router-bootstrap';
// import Button from '@material-ui/core/Button';
// import { Navbar } from 'reactstrap';
// import { BrowserRouter as Link } from "react-router-dom";
import '../styles/header_style.css';

function Header(){

  
  const testEmail = JSON.parse(localStorage.getItem('user info'));
    

  function Logout(event){
    localStorage.removeItem("token");
    localStorage.removeItem("user info");
    localStorage.removeItem("email");
}

  return(
    // const useStyles = makeStyles({
    //     create: {
    //       background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
    //       border: 0,
    //       borderRadius: 3,
    //       boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
    //       color: 'white',
    //       height: 24,
    //       padding: '0 30px',
    //     },
    //   });
    
    // const classes = useStyles();


    

    <div class="container">
    <header class="d-flex flex-wrap justify-content-center py-3 mb-4 border-bottom">
      <a href="/" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-dark text-decoration-none">
        <svg class="bi me-2" width="40" height="32"></svg>
        <span class="fs-4" style={{ "font-family": 'Glory' }}><h1><span style={{ "color": "blue"}}>Toge</span><span style={{ "color": "hotpink"}}>ther</span></h1></span>
      </a>

      <ul class="nav nav-pills">
        {testEmail ? (
          <div>
            <li class="nav-item"><a href="/wedding">Weddings</a></li>
            <li class="nav-item"><a href="/chat">Chat</a></li>
            <li class="nav-item"><a href="/" onClick={Logout}>Logout</a></li>
          </div>
        ) : (
          <div>
            <li class="nav-item"><a href="/">Login</a></li>
          </div>
        )}
      </ul>
    </header>
  </div>
  )
    // <header className="b-example-divider">
    //   <h1 style={{ "font-family": 'Glory' }}>Together</h1>
    //     <Link to="/weddings"><Button className={classes.create}>Weddings Page</Button></Link>
    //     <Link to="/"><Button className={classes.create} onClick={Logout}>Logout</Button></Link>
    // </header>
}

export default Header;