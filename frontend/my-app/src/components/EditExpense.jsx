import axios from "axios";
import { useState, useRef } from "react";
import { useParams } from "react-router";


export default function EditExpense(props){
    const [expenses, setExpenses] = useState([]);
    const expenseInput = useRef(null);
    const reasonInput = useRef(null);
    const weddingId = useRef(null);
    const expenseId = useRef(null);
    
    const { id } = useParams();

    async function getExpense(id){
        const response = await axios.get(`http://34.66.174.184:3004/expenses/${id}`)
        setExpenses(response.data);
        console.log(setExpenses(response.data));
    }

    getExpense(id);
    
    async function editExpense(event){
        const newExpense = {
            expenseId: expenses.expenseId,
            expenseAmount: expenseInput.current.value,
            reason: reasonInput.current.value,
            weddingId: expenses.weddingId
        }
        console.log(expenseId);
        console.log(weddingId);
        const response = await axios.put(`http://34.66.174.184:3004/expenses/${id}`, newExpense);
        alert("Expense Edited!");
        setExpenses(response.data);
    }

    return(
        <div>
            <input placeholder={expenses.expenseAmount} ref={expenseInput}></input>
            <input placeholder={expenses.reason} ref={reasonInput}></input>
            <button onClick={editExpense}>Edit Expense 💲</button>
        </div>
    )
}

    