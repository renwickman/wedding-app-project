import axios from "axios";
import { useRef } from "react";
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

export default function CreateWedding(){

    const groomInput = useRef(null);
    const brideInput = useRef(null);
    const locationInput = useRef(null);
    const budgetInput = useRef(null);
    const dateInput = useRef(null);

    const useStyles = makeStyles({
        create: {
          background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
          border: 0,
          borderRadius: 3,
          boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
          color: 'white',
          height: 24,
          padding: '0 30px',
        },
      });
    
      const classes = useStyles();

    async function addWedding(){
        const wedding = {
            groomName: groomInput.current.value,
            brideName: brideInput.current.value,
            weddingLocation: locationInput.current.value,
            weddingBudget: budgetInput.current.value,
            weddingDate: dateInput.current.value
        }
        const response = await axios.post("http://34.66.174.184:3004/weddings", wedding);
        alert("New Wedding Added!");
    }


    return(<div style={{ "font-family": 'Glory' }}>
        <h2>Wedding Form</h2>

            <input placeholder="groom" ref={groomInput} type="text" required></input>
            <input placeholder="bride" ref={brideInput} type="text" required></input>
            <select placeholder="location" ref={locationInput} required>
                <option value="Dallas, TX">Dallas, TX</option>
                <option value="Los Angeles, CA">Los Angeles, CA</option>
                <option value="New York City, NY">New York City, NY</option>
                <option value="Detroit, MI">Detroit, MI</option>
                <option value="Miami, FL">Miami, FL</option>
                <option value="Austin, TX">Austin, TX</option>
                <option value="St. Louis, MO">St. Louis, MO</option>
                <option value="Chicago, IL">Chicago, IL</option>
                <option value="Houston, TX">Houston, TX</option>
                <option value="Atlanta, GA">Atlanta, GA</option>
                <option value="Las Vegas, NV">Las Vegas, NV</option>
                <option value="Seattle, WA">Seattle, WA</option>
            </select>
            <input placeholder="budget" ref={budgetInput} type="number" required></input>
            <input placeholder="date" ref={dateInput} type="date" required></input>
            <Button className={classes.create} onClick={addWedding}>Add that wedding</Button>
        </div>)
}