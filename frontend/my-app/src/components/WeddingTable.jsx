import axios from "axios";

export default function WeddingTable(props){
     const weddings = props.weddings;
     const getWeddings = props.getAllWeddings; 

    async function deleteWedding(event){
        console.log(event);
        const weddingId = event.target.getAttribute('weddingid');
        const response = await axios.delete(`http://34.66.174.184:3004/weddings/${weddingId}`);
        getWeddings();
    }


    return(
        
        <table style={{ "font-family": 'Glory' }}>
            <thead><tr><th>ID</th><th>Wedding</th><th>Location</th><th>Budget</th><th>Date</th></tr></thead>
            <tbody>
                {weddings.map(w => <tr key={w.weddingId}>
                    <a href={`getwedding/${w.weddingId}`}><td>{w.weddingId}</td></a>
                    <td>{w.groomName} 🤵 & {w.brideName} 👰 </td>
                    <td>{w.weddingLocation} 📍 </td>
                    <td>${w.weddingBudget} 💰 </td>
                    <td>{new Date(w.weddingDate).toDateString()} 📅</td>
                    <td><a href={`addexpense/${w.weddingId}`}>Add Expense 💲 </a></td>
                    <td><a href={`editwedding/${w.weddingId}`}>Edit Wedding 💒 </a></td>
                    <td><button weddingid = {w.weddingId} onClick={deleteWedding}>❌</button></td>
            </tr>)}
            </tbody>
        </table>)
}