import axios from "axios";
import { useRef, useState, useEffect } from "react";
import { useParams } from "react-router";
import ExpenseTable from "./ExpenseTable";
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

export default function CreateExpense(){

    const { id } = useParams();
    const [wedding, setWedding] = useState([]);
    const [expenses, setExpenses] = useState([]);
    const expenseInput = useRef(null);
    const reasonInput = useRef(null);
    const [spent,setSpent] = useState(0);

    const useStyles = makeStyles({
        create: {
          background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
          border: 0,
          borderRadius: 3,
          boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
          color: 'white',
          height: 24,
          padding: '0 30px',
        },
      });
    
      const classes = useStyles();
    
    async function getWedding(id){
        const response = await axios.get(`http://34.66.174.184:3004/weddings/${id}`);
        const weddingData = response.data;
        setWedding(weddingData);
    }

    useEffect(() => {getWedding(id)}, [id]);

    async function getAllExpenses(event){
        const response = await axios.get(`http://34.66.174.184:3004/weddings/${id}/expenses`);
        setExpenses(response.data);  
        let sum = 0;
        for(const ex of response.data ){
            sum += ex.expenseAmount;
        }
        setSpent(sum)
    }

    
    async function addExpense(event){
        const expense = {
            expenseAmount: expenseInput.current.value,
            reason: reasonInput.current.value,
            weddingId: Number(id)
        }
        const response = await axios.post(`http://34.66.174.184:3004/weddings/${id}/expenses`, expense);
        alert("New Expense Added!");
    }


    return(<div style={{ "font-family": 'Glory' }}>
        <h2>{wedding.groomName} & {wedding.brideName}</h2>
        <ul>
            <li>${wedding.weddingBudget}</li>
            <li>${spent}</li>
            <li></li>
            <li></li>
            <li>Total:</li>
            <li>${wedding.weddingBudget - spent}</li>
        </ul>
        <br />
        <hr />
        <br />
        <h2>Current Expenses</h2>
        <Button className={classes.create} onClick={getAllExpenses}>Get Expenses</Button>
        <ExpenseTable expenses={expenses} getAllExpenses={getAllExpenses}>
        </ExpenseTable>
        <br />
        <hr />
        <br />
        <h2>Expense Form</h2>
            <input placeholder="expense" ref={expenseInput} type="number" required></input>
            <input placeholder="reason" ref={reasonInput} type="text" required></input>
            <Button className={classes.create} onClick={addExpense}>Add expense</Button>
        </div>)
}