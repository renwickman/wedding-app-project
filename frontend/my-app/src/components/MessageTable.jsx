// import { uploadPhoto } from "../../../../backend/Upload";

export default function MessageTable(props){
    const messages = props.messages;
    const getMessages = props.getAllmessages; 

    getMessages();
    
    return(<table><tr><th>Message</th><th>Sender</th><th>Recipient</th></tr>
        <tbody>
        {messages.map(m => <tr key={m.messageId}>
                    <td>{m.note}</td>
                    <td>{m.sender}</td>
                    <td>{m.recipient}</td>
                    </tr>)}
        </tbody>
        </table>)
    }