
export default function NotFound(){

    return(<div>
        <h1>404</h1>
        <h2>Page Not Found!</h2>

        <h4><a href="/">Go Back</a></h4>
    </div>)
}