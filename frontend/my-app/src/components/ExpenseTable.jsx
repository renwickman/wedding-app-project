import axios from "axios";
import { useState } from "react";

export default function ExpenseTable(props){
    const expenses = props.expenses;
    const getExpenses = props.getAllExpenses; 

    async function deleteExpense(event){
        console.log(event);
        const expenseId = event.target.getAttribute('expenseid');
        console.log(expenseId);
        const response = await axios.delete(`http://34.66.174.184:3004/expenses/${expenseId}`);
        getExpenses();
    }

    //contentType
    //setResponse
    async function uploadPhotoNow(contentType, data, setResponse){
        await axios({
            url: 'https://us-central1-renwick-wedding-app.cloudfunctions.net/upload-function',
            method: 'POST',
            data: data,
            headers: {
                'Content-Type': contentType
            }
            }).then((response) => {
                setResponse(response.data);
            }).catch((error) => {
                setResponse(error);
            })
        }

        const [fileName, setName] = useState("");
        const [file, setFile] = useState(null);
        const [ext, setExt] = useState("");


    async function base64(){

        const toBase64 = file => new Promise((resolve, reject) =>{
            let reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => resolve(reader.result);
            reader.onerror = error => reject(error);
        });

        const data = {
            name: fileName,
            content: await toBase64(file),
            extension: ext
        }

        uploadPhotoNow("application/json", data, (msg) => console.log(msg));
    }


    return(<div><table><tr><th>Amount</th><th>Reason</th><th>Edit</th><th>Delete</th></tr>
    <tbody>
    {expenses.map(e => <tr key={e.expenseId}>
                <td>${e.expenseAmount}</td>
                <a href={`/getexpense/${e.expenseId}`}><td>{e.reason} 📝</td></a>
                <td><a href={`/editexpense/${e.expenseId}`}>Edit Expense💲</a></td>
                <td><button expenseid={e.expenseId} onClick={deleteExpense}>❌</button></td>
                </tr>)}
        </tbody>
    </table>

    <hr/>

        <input
        type="text"
        name = "fileName"
        onChange={e => setName(e.target.value)} 
        required/>
        <input
        type="file"
        name = "file"
        onChange={event => setFile(event.target.files[0])} 
        required/>
        <select name="ext" placeholder="extension" onChange={e => setExt(e.target.value)} required>
                <option>--</option>
                <option value="jpg">jpg</option>
                <option value="png">png</option>
        </select>
        <button onClick={base64}>Upload Photo</button>

        </div>)
}