import axios from "axios";
import { useRef, useState } from "react";
import '../styles/login_style.css';



export default function Login(){

    
    const emailInput = useRef(null);
    const passwordInput = useRef(null);
    const [error, setError] = useState("");

    async function submitLogin(event){

        const login = {
            email: emailInput.current.value,
            password: passwordInput.current.value,
        }

        const response = await axios.patch('https://authorization-service-dot-renwick-wedding-app.ue.r.appspot.com/employees/login', login);
        console.log(response);
        if (response){
            const redirect = await axios.get(`http://authorization-service-dot-renwick-wedding-app.ue.r.appspot.com/employees/${login.email}/verify`);
            if (redirect){
                localStorage.setItem("user info", response.config.data);
                console.log(localStorage.getItem("user info"));
                window.location.href='/wedding'
            } else {
                setError("Login Failed II.  Try Again.");
                window.location.href='/'
            }
        } else {
            setError("Login Failed.  Try Again.");
            window.location.href='/'
        }
        
    }

    return(<div style={{ "font-family": 'Glory' }}>
        <body class="text-center">
            <main class="form-signin">
                <h2 style={{ "font-family": 'Glory' }}>Email</h2>
                <div class="form-floating">
            <label htmlFor="emailInput"></label>
            <input name="emailInput" type="email" id="floatingInput" class="form-control" placeholder="email" ref={emailInput} />
        </div>

            <br />

        <h2 style={{ "font-family": 'Glory' }}>Password</h2>
        <div class="form-floating">
            <label htmlFor="passwordInput"></label>
            <input name="passwordInput" type="password" id="floatingPassword" class="form-control" placeholder="password" ref={passwordInput}/>
        </div>
            <button class="w-100 btn btn-lg btn-primary" type="submit" onClick={submitLogin}>Login</button>
            <p class="mt-5 mb-3 text-muted">&copy; Anthony Renwick</p>
            <h4>{error}</h4>
        </main>
        </body>
    </div>)
}