import axios from "axios";
import { useParams } from 'react-router';
import { useState } from "react";

export default function GetWedding(){

    const {id} = useParams();
    const [wedding, setWedding] = useState([]);

    async function getWedding(id){
        const response = await axios.get(`http://34.66.174.184:3004/weddings/${id}`);
        console.log(response);
        setWedding(response.data);
    }

    getWedding(id);

    return(<div style={{ "font-family": 'Glory' }}>
        <h1 style={{ "color": "blue"}}>{wedding.groomName} 🤵 </h1> & <h1 style={{ "color": "hotpink"}}> 👰 {wedding.brideName}</h1>
        <h3>{wedding.weddingLocation} 📍</h3>
        <h3>${wedding.weddingBudget} 💰</h3>
        <h3>{new Date(wedding.weddingDate).toDateString()} 📅</h3>
    </div>)
}