import axios from "axios";
import { useState } from "react";
import WeddingTable from "./WeddingTable";


export default function Wedding(){

    const [weddings, setWeddings] = useState([]);

    async function getWeddings(event){
        const response = await axios.get('http://34.66.174.184:3004/weddings');
        setWeddings(response.data);
    }

    return(<div>
        <h2 style={{ "font-family": 'Glory' }}>Wedding Page</h2>
        <button type="button" class="btn btn-outline-primary"><a href="/chat" style={{ "font-family": 'Glory' }}>Chat Room</a></button>
        <button type="button" class="btn btn-outline-warning"><a href="/addwedding" style={{ "font-family": 'Glory' }}>Create Wedding</a></button>
        <button type="button" class="btn btn-outline-danger" onClick={getWeddings} style={{ "font-family": 'Glory' }}> Get the Weddings</button>
        <WeddingTable weddings={weddings} getAllWeddings={getWeddings}></WeddingTable>
    </div>)
}