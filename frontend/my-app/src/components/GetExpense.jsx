import axios from "axios";
import { useParams } from 'react-router';
import { useState } from "react";

export default function GetExpense(){

    const {id} = useParams();
    const [expense, setExpense] = useState([]);

    async function getExpense(id){
        const response = await axios.get(`http://34.66.174.184:3004/expenses/${id}`);
        console.log(response);
        setExpense(response.data);
    }

    getExpense(id);

    return(<div style={{ "font-family": 'Glory' }}>
        <h1>Expense Amount:</h1>
        <h1>${expense.expenseAmount}</h1>
        <br/>
        <br/>
        <h1>Reason:</h1>
        <h1> {expense.reason}</h1>
        
    </div>)
}